# Paginación efectiva con AdonisJS

Resultado del tutorial: [#004 - Paginación efectiva con AdonisJS](http://www.victorvr.com/tutorial/paginacion-efectiva-con-adonisjs)

Demo: [https://adonisjs-pagination.herokuapp.com](https://adonisjs-pagination.herokuapp.com)

![App](http://www.victorvr.com/img/posts/Post-04.png)

# Instalación de la aplicación en Heroku

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

## Instalación de Heroku CLI

1.- Primero necesitamos instalar `Heroku CLI`.

```bash
npm install -g heroku
```

2.- Verificamos la instalación `heroku >= 7.0` o mayor.

```bash
heroku --version
```

## Instalación de la aplicación en Heroku

1.- Ingresar al directorio de la aplicación `adonisjs-pagination`.

```bash
cd adonisjs-pagination
```

2.- Iniciar sesión en Heroku con los datos de nuestra cuenta.

```bash
heroku login -i
```

3.- Crea la aplicación `adonisjs-pagination` en Heroku.

```bash
heroku create adonisjs-pagination
```

4.- Agregar el repositorio remoto de Heroku a nuestro repositorio local.

```bash
heroku git:remote -a adonisjs-pagination
```

5.- Agregar las siguientes variables de configuración.

```bash
heroku config:set HOST=::
heroku config:set APP_URL=https://adonisjs-pagination.herokuapp.com
```

6.- Agregar `JawsDB` (soporte para `MySQL`) a nuestra aplicación.

```bash
heroku addons:create jawsdb
```

7.- Visualizar las variable de configuración `JAWSDB_URL`.

```bash
heroku config:get JAWSDB_URL
```

8.- Agregar las siguientes variables de configuración en base a la variable `JAWSDB_URL`:

`JAWSDB_URL=mysql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:3306/{DB_DATABASE}`

```bash
heroku config:set DB_CONNECTION=mysql
heroku config:set DB_HOST={DB_HOST}
heroku config:set DB_PORT=3306
heroku config:set DB_USER={DB_USER}
heroku config:set DB_PASSWORD={DB_PASSWORD}
heroku config:set DB_DATABASE={DB_DATABASE}
```

9.- Visualizar las variables de configuración.
```bash
heroku config
```

10.- Implementar nuestra aplicación en Heroku.

```bash
git push heroku master
```

11.- Ejecutar las migraciones en la base de datos de la aplicación.
```bash
heroku run node ace migration:run --force
heroku run node ace seed --files=EmployeeSeeder.js --force
```

12.- Abrir la aplicación en el navegador.
```bash
heroku open
```