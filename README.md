# Paginación efectiva con AdonisJS

Resultado del tutorial: [#004 - Paginación efectiva con AdonisJS](http://www.victorvr.com/tutorial/paginacion-efectiva-con-adonisjs)

Demo: [https://adonisjs-pagination.herokuapp.com](https://adonisjs-pagination.herokuapp.com)

![App](/public/app.gif)

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

## Instalación de Adonis CLI

Primero necesitamos instalar `Adonis CLI`:

```bash
npm i -g adonisjs-cli
```

## Instalación de Dependencias via NPM

Ahora instalaremos las dependencias de nuestra aplicación:

```bash
npm install
```

## Crear base de datos en MySQL

Continuamos creando la base de datos en `MySQL` llamada `adonisjs-pagination`:

```bash
mysql -u usuario -p
mysql > CREATE DATABASE adonisjs-pagination;
```

## Configurar variables de entorno

Configuramos las variables de entorno del archivo `.env`:

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER={TU_USUARIO}
DB_PASSWORD={TU_CONTRASEÑA}
DB_DATABASE=adonisjs-pagination
```

## Ejecutar migración

Ejecutamos la migracion de la base de datos:

```bash
adonis migration:run
```

## Ejecutar seed
Llenamos la base de datos con datos de prueba:

```bash
adonis seed --files=EmployeeSeeder.js
```

## Iniciar el servidor
Ejecutamos la aplicación:

```bash
adonis serve --dev
```

## Abrir la aplicación
Y por último, abrimos [http://localhost:3333](http://localhost:3333) en el navegador.