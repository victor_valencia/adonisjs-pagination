'use strict'

const Employee = use('App/Models/Employee')

class EmployeeController {

  async index ({ view, params, request, response }) {
    const page = params.page || 1
    const search = request.input('search') || ''
    const employees = await Employee.query()
                                    .where('name', 'LIKE', '%' + search + '%')
                                    .paginate(page, 10)
    const pagination = employees.toJSON()
    pagination.route = 'employees.pagination'
    if(pagination.lastPage < page && page != 1) {
      response.route(pagination.route, { page: 1 }, null, true)
    }
    else {
      pagination.offset = (pagination.page - 1) * pagination.perPage
      pagination.search = search
      return view.render('employees.index', { employees: pagination })
    }
  }

}

module.exports = EmployeeController
