'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/Employee', (faker, i, data) => {
  return {
    id: data.id || (i+1),
    name: data.name || faker.name() + ' ' + faker.last(),
    email: data.email || faker.word() + '@' + faker.domain(),
  }
})
