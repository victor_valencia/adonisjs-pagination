'use strict'

/*
|--------------------------------------------------------------------------
| EmployeeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Employee = use('App/Models/Employee')

class EmployeeSeeder {
  async run () {
    //Truncar la tabla 'employees' en la base de datos
    await Employee.truncate()
    //Crear todos los empleados (95 registros en total)
    for(var i = 1; i <= 95; i++){
      await Factory.model('App/Models/Employee').create({
        id: i,
      })
    }
  }
}

module.exports = EmployeeSeeder
